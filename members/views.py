from django.shortcuts import render
from .models import *
from django.http import JsonResponse
from django.core import serializers

def members(request):
    mems = Member.objects.all()
    response = {}
    for i in range(len(mems)):
        serialized_obj = serializers.serialize('python', [mems[i], ])[0]['fields']
        if (serialized_obj['image'] != ''):
            serialized_obj['image'] = mems[i].image.url[:-16]
        response[i] = serialized_obj
    return JsonResponse(response)
