from django.db import models
from gdstorage.storage import GoogleDriveStorage

# Define Google Drive Storage
gd_storage = GoogleDriveStorage()

class Member(models.Model):
    name = models.CharField(max_length=200)
    division = models.CharField(max_length=200, choices=(('Electrical', 'Electrical'), ('Mechanic', 'Mechanic'), ('Artificial Intelligence', 'Artificial Intelligence'), ('Managerial', 'Managerial')))
    major = models.CharField(max_length=200)
    year = models.IntegerField()
    image = models.ImageField(upload_to='amvui-api-v1/members/', storage=gd_storage, blank=True)
    linkedin = models.URLField(max_length=200, blank=True)
    instagram = models.URLField(max_length=200, blank=True)
    github = models.URLField(max_length=200, blank=True)
    gitlab = models.URLField(max_length=200, blank=True)
    facebook = models.URLField(max_length=200, blank=True)
    twitter = models.URLField(max_length=200, blank=True)
    
    def __str__(self):
        return self.name + ' - ' + self.division + ' - ' + self.major + ' ' + str(self.year)
