from django.shortcuts import render
from .models import *
from django.http import JsonResponse
from django.core import serializers

def robots(request):
    robs = Robot.objects.all()
    response = {}
    for i in range(len(robs)):
        serialized_obj = serializers.serialize('python', [robs[i], ])[0]['fields']
        docs = RobotDocumentation.objects.filter(pk__in=serialized_obj['documentation'])
        for j in range(len(docs)):
            serialized_obj['documentation'][j] = docs[j].image.url[:-16]
        response[i] = serialized_obj
    return JsonResponse(response)
