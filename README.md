# AMV UI Backend

---

## Deskripsi
Backend dari website amv ui

## Models
* `CompetitionDocumentation`<br>
  Gambar-gambar dokumentasi dari suatu kompetisi<br>
  Fields:
  * `name`: nama gambar
  * `image`: gambar dokumentasi
* `Competition`<br>
  Lomba-lomba yang diikuti AMV UI
  Fields:
  * `name`: nama dari lomba yang diikuti
  * `description`: deskripsi singkat, maksimal 2200 karakter
  * `location`: lokasi lomba (opsional)
  * `year`: tahun perlombaan (opsional)
  * `documentation`: gambar dokumentasi dari perlombaan, satu perlombaan bisa memiliki banyak gambar (opsional)
  * `created_at`: timestamp saat data dimasukkan ke database
* `Member`<br>
  Gambar-gambar dokumentasi dari robot yang dibuat<br>
  Fields:
    * `name`: nama anggota
    * `division`: divisi dari anggota, pilihannya ada `Electrical`, `Mechanic`, `Artificial Intelligence`, dan `Managerial`
    * `major`: jurusan anggota
    * `year`: tahun angkatan anggota
    * `image`: gambar profil (opsional)
    * `linkedin`: alamat linkedin (opsional)
    * `instagram`: alamat instagram (opsional)
    * `github`: alamat github (opsional)
    * `gitlab`: alamat gitlab (opsional)
    * `facebook`: alamat facebook (opsional)
    * `twitter`: alamat twitter (opsional)
* `RobotDocumentation`<br>
  Gambar-gambar dokumentasi dari robot yang dibuat<br>
  Fields:
  * `name`: nama gambar
  * `image`: gambar dokumentasi
* `Robot`<br>
  Lomba-lomba yang diikuti AMV UI
  Fields:
  * `name`: nama dari robot yang dibuat
  * `description`: deskripsi singkat, maksimal 2200 karakter
  * `documentation`: gambar dokumentasi dari suatu robot, bisa memiliki banyak gambar (opsional)
  * `created_at`: timestamp saat data dimasukkan ke database

## Direktori
Data dapat diambil dalam bentuk JSON melalui direktori-direktori berikut:
* `/competitions`<br>
  Menampilkan seluruh data dari models `Competition`
* `/members`<br>
  Menampilkan seluruh data dari models `Member`
* `/robots`<br>
  Menampilkan seluruh data dari models `Robot`
