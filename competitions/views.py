from django.shortcuts import render
from .models import *
from django.http import JsonResponse
from django.core import serializers

def competitions(request):
    comps = Competition.objects.all()
    response = {}
    for i in range(len(comps)):
        serialized_obj = serializers.serialize('python', [comps[i], ])[0]['fields']
        docs = CompetitionDocumentation.objects.filter(pk__in=serialized_obj['documentation'])
        for j in range(len(docs)):
            serialized_obj['documentation'][j] = docs[j].image.url[:-16]
        response[i] = serialized_obj
    return JsonResponse(response)
