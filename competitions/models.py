from django.db import models
from gdstorage.storage import GoogleDriveStorage

# Define Google Drive Storage
gd_storage = GoogleDriveStorage()

class CompetitionDocumentation(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='amvui-api-v1/', storage=gd_storage)
    def __str__(self):
        return self.name

class Competition(models.Model):
    name = models.CharField(max_length=200)
    description = models.CharField(max_length=2200)
    location = models.CharField(max_length=200, blank=True, null=True)
    year = models.IntegerField(blank=True, null=True)
    documentation = models.ManyToManyField(CompetitionDocumentation, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    def __str__(self):
        return self.name
